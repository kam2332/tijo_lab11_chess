package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.chess.RulesOfGame;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.service.ChessService;

import java.util.Arrays;
import java.util.List;

@Service
public class ChessServiceImpl implements ChessService {

    private RulesOfGame bishop;
    private RulesOfGame knight;
    private RulesOfGame king;
    private RulesOfGame queen;
    private RulesOfGame rock;
    private RulesOfGame pawn;

    @Autowired
    public ChessServiceImpl(@Qualifier("Bishop") RulesOfGame bishop,
                            @Qualifier("Knight") RulesOfGame knight,
                            @Qualifier("King") RulesOfGame king,
                            @Qualifier("Queen") RulesOfGame queen,
                            @Qualifier("Rock") RulesOfGame rock,
                            @Qualifier("Pawn") RulesOfGame pawn) {
        this.bishop = bishop;
        this.knight = knight;
        this.king = king;
        this.queen = queen;
        this.rock = rock;
        this.pawn = pawn;
    }

    @Override
    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {

        List<String> startPosition = Arrays.asList(figureMoveDto.getStart().split("_"));
        List<String> destinationPosition = Arrays.asList(figureMoveDto.getDestination().split("_"));

        int xStart = mapToInt(startPosition.get(0));
        int xEnd = mapToInt(destinationPosition.get(0));

        int yStart = mapToInt(startPosition.get(1));
        int yEnd = mapToInt(destinationPosition.get(1));

        switch(figureMoveDto.getType()) {
            case KING:
                return king.isCorrectMove(xStart, yStart, xEnd, yEnd);
            case QUEEN:
                return queen.isCorrectMove(xStart, yStart, xEnd, yEnd);
            case ROCK:
                return rock.isCorrectMove(xStart, yStart, xEnd, yEnd);
            case BISHOP:
                return bishop.isCorrectMove(xStart, yStart, xEnd, yEnd);
            case KNIGHT:
                return knight.isCorrectMove(xStart, yStart, xEnd, yEnd);
            case PAWN:
                return pawn.isCorrectMove(xStart, yStart, xEnd, yEnd);
            default:
                return false;
        }
    }

    private Integer mapToInt(String s) {
        return s.toLowerCase().getBytes()[0] - 97;
    }
}
